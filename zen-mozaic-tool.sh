#!/bin/bash
# vim: set foldmethod=marker foldmarker=--8<--,-->8-

readonly LICENCE_VERSION_TXT="
zen-mozaic-tool.sh  1.0.0
Copyright Risto Karola 2022.
Licensed under the EUPL-1.2.
You may obtain a copy of the licence at
https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
"

readonly HELP_TXT="
Tool for handling data in Creative Zen Mozaic MP3 player.

USAGE
   zen-mozaic-tool.sh [options] [command [parameter(s)]]

OPTIONS
   --copyright     display version, copyright and licence information and exit
   -h, --help      display this help text and exit
   --licence       display version, copyright and licence information and exit
   -r, --reset-usb reset usb connection before detecting Zen
   --version       display version, copyright and licence information and exit


COMMANDS AND PARAMETERS
   If no command is given, the script operates interactively.

   create-folder NAME PARENT-ID
       Create new folder in Zen under folder defined by PARENT-ID, use
       PARENT-ID = 0 to create on the root level.  Name the folder as NAME.

   delete-id ID
       Delete file or folder in Zen; the file/folder is defined by ID.
       In case of folder, everything under it will also be deleted.

   get-file ID [PATH]FILE-NAME
       Download file with ID from Zen and store it as [PATH]FILE-NAME.
       The FILE is not downloaded, if [PATH]FILE-NAME already exists.

   list-albums PERFORMER-ID
       An alias for list-folders PERFORMER-ID.

   list-folders [FOLDER-ID]
       Show Zen folders under folder FOLDER-ID.  If FOLDER-ID is not given or
       FOLDER-ID = 0, then display all folders.

   list-music
       Lists folders and files under folder /Music in Zen.

   list-performers
       List music performers in Zen.  This is listing of the first level
       subfolders under folder /Music.

   list-tree [FOLDER-ID]
       Show Zen folders & files under folder FOLDER-ID.  If FOLDER-ID is not
       given or FOLDER-ID = 0, then display all folders and files.

   load-album [PATH]FILE
       Convert music in album FILE.flac to MP3 files and load them to folder
       /Music/<performer>/<album>/ in Zen.  Create also an album file in Zen:
       /My Albums/<album>.alb.  If album art FILE-art.jpg exists, it is
       connected to the album file.  This process requires existence of the
       cue sheet FILE.cue.

   load-art ALBUM-FILE [PATH]ALBUM-ART-FILE
       Connect the album art image ALBUM-ART-FILE to an album file in Zen.  The
       album file must already exist in Zen: /My Albums/ALBUM-FILE.

   load-track [PATH]FILE TITLE ARTIST ALBUMARTIST ALBUM GENRE TRACKNUM YEAR
       Load a music track FILE to Zen's path
       /Music/ALBUMARTIST/[YEAR: ]ALBUM/[TRACKNUM: ]TITLE.<extension>.
       Create album file /My Albums/ALBUM.alb, if it doesn't already exist.
       - FILE: music track file.
       - TITLE: track title.  This string must not be null.
       - ARTIST/ALBUMARTIST: track/album performer.  At least one of them is
         needed, thus both of them must not be a null strings.
       - ALBUM: album name.  This string must not be null.
       - GENRE: music genre.
       - TRACKNUM: track number in the album.
       - YEAR: album year.
"

declare -a ZNAME      # One-based array of file names in the file listing
declare -a ZID        # One-based array of file ID numbers
declare -a ZTYPE      # One-based array of file types
declare -a ZLEVEL     # One-based array of filetree depths
declare -a ZID2INDEX  # Array: ID number to array index

MUSICID=0             # Will be filled with the ID of the folder /Music
TERMWIDTH=80          # Terminal width, will be set with tput command later
SPACES=' '            # String of spaces, will be expanded to >=TERMWIDTH later


# --- message() title messageline1 [messageline2 ...]] -------------------8<--1
# INFO
#    Display the parameters $2, $3,... as an indented message.  Each parameter
#    is displayed on it's own line.  If a parameter is longer than terminal
#    width, the text is split on two (or more) lines.  The prefix $1 is shown
#    on the first line, length of title determines the indentation.
#
#    title messageline1
#          messageline1
#          ...
# INPUT
#    $1 (str) the title displayed on the first line
#    $2 (str) first line message
#    $3 (str) optional 2nd line of message
#    $N...    optional...
# OUTPUT
#    Stdout   display the message
#    Stderr   none
#    File     none
#    Variable none
message() {
    local msg
    local prefix="$1"

    while [ -n "$2" ]; do
        msg="$prefix $2"
        prefix="${SPACES:0:${#prefix}}"
        while [ "${#msg}" -gt "$TERMWIDTH" ]; do
            printf '%s\n' "${msg:0:TERMWIDTH}"
            msg="$prefix ${msg:TERMWIDTH}"
        done
        msg="$msg$SPACES"
        printf '%s\n' "${msg:0:TERMWIDTH}"
        shift
    done
}


# --- info() text1 [text2 [text3 ...]] -----------------------------------8<--1
# INFO
#    Show "INFO: " and given parameters as indented text.  Each parameter is
#    shown on it's own line using yellow color.
# INPUT
#    $@ (str) the information text
# OUTPUT
#    Stdout   show the text, each parameter indented on separate line
#    Stderr   none
#    File     none
#    Variable none
info() {
    printf '\e[93m%s\e[0m\n' "$(message INFO: "$@")"
}


# --- error() messageline1 [messageline2 [messageline3...]] --------------8<--1
# INFO
#    Display the parameters as an error message (stderr) and exit with code 1.
#    Text is displayed on red background.  If a parameter is longer than
#    terminal width, the text is split on two (or more) lines.
# INPUT
#    $1 (str) the first line of message:    "Error: MESSAGE TEXT"
#    $2 (str) optional 2nd line of message: "       SECOND MESSAGE LINE"
#    $3 (str) optional 3rd line of message: "       THIRD LINE"
#    $N...    optional...
# OUTPUT
#    Stdout   none
#    Stderr   show the error message
#    File     none
#    Variable none
error() {
    printf '\e[41m%s\e[0m\n' "$(message ERROR: "$@")" >&2
    exit 1
}


# --- errorNum() errornumber [str] ---------------------------------------8<--1
# INFO
#    Show error number $1.  Some errors use additional info string $2, which is
#    shown at the end of the error message.
# INPUT
#    $1 (int) the number of the displayed error message
#    $2 (str) extra error message information
# OUTPUT
#    Stdout   none
#    Stderr   show the error message
#    File     none
#    Variable none
errorNum() {
    case "$1" in
        1) error "Folder is not found: $2"          ;;
        2) error 'Too many parameters given!'       ;;
        3) error "Expecting parameter: $2"          ;;
        4) error "File is not found: $2"            ;;
        5) error "This is not a folder: $2"         ;;
        6) error "The ID number does not exist: $2" ;;
        7) error "Unknown option: $2"               ;;
        *) error "Unknown error number: $1"         ;;
    esac
}


# --- cmd() returnvar command parameters ---------------------------------8<--1
# INFO
#    Show "CMD: " and $2, $3,... with yellow color. Execute $2 with parameters
#    $3, $4,...  If $1 is non-empty, returns the output of the command to
#    variable named $1.
# INPUT
#    $1 (str) caller's variable name, where the return data is written
#    $2 (str) the command
#    $3,$4... parameters
# OUTPUT
#    Stdout   show the command, and if $1='', then show the command output
#    Stderr   none
#    File     none
#    Variable none
cmd() {
    local c="$2"
    local outputToVar='false'

    if [ -n "$1" ]; then
        local -n _returnvar="$1"  # Set the nameref
        outputToVar='true'
    fi

    shift 2
    printf '\e[93m%s\e[0m\n' "$(message 'CMD: ' "$c $*")"
    if [ "$outputToVar" = 'true' ]; then
        _returnvar="$("$c" "$@" 2>&1)"
    else
        "$c" "$@"
    fi
}


# --- albumData() returnvar cuefile data ---------------------------------8<--1
# INFO
#    Search string $3 in file $2.  Return string after $3 on the same line.
#    If the string starts with a double-quote, return string between the first
#    and the last double-quotes.  The string is returned to the variable,
#    which name is given as $1.
#    first match.
# INPUT
#    $1 (str) caller's variable name, where the return data is written
#    $2 (str) the file to be used in searching
#    $3 (str) the first string to be searched
#    $4 (str) the second string to be searched
# OUTPUT
#    Stdout   none
#    Stderr   none
#    File     none
#    Variable store the rest of line after $STR2 match to variable named $1
albumData() {
    local -n _returnvar="$1"  # Nameref to caller's variable
    local d

    if [ ! -f "$2" ]; then errorNum 4 "$2"; fi  # Error if file doesn't exist

    # Sed writes to $d the end of the first maching line.
    # Then spaces are trimmed, and if the string starts with double-quotes
    # then also first and last double-quote are removed.
    d="$(sed -n -E "0,/$3/ s/.*$3[[:space:]]+(.*)/\1/ p" "$2")"
    if [ "${d:0:1}" = '"' ]; then
        d="${d:1}"    # Drop the first character ("), and the last
        d="${d%\"*}"  #   double-quote and everything after it
    else
        d="$(sed -E 's/[[:space:]]*$//' <<<"$d")"  # Remove trailing space
    fi
    _returnvar="$d"
}


# --- trackData() returnvar cuefile tracknumber data ---------------------8<--1
# INFO
#    Search cue file $2 track $3 data $4.  Return string after $4 on the line.
#    If the string starts with a double-quote, returne string between the first
#    and the last double-quotes.  The string is returned to the variable,
#    which name is given as $1.
# INPUT
#    $1 (str) caller's variable name, where the return data is written
#    $2 (str) the file to be used in searching
#    $3 (str) the first string to be searched
#    $4 (str) the second string to be searched
# OUTPUT
#    Stdout   none
#    Stderr   none
#    File     none
#    Variable store the rest of line after $STR2 match to variable named $1
trackData() {
    local -n _returnvar="$1"  # Nameref to caller's variable
    local d
    local number="$((10#$3))"  # Track number without leading zeros
    local range="/TRACK[[:blank:]]+0*$number([[:space:]]|$)/,/TRACK/"

    if [ ! -f "$2" ]; then errorNum 4 "$2"; fi  # Error if file doesn't exist

    # Sed writes to $d the end of the line after the match $4.
    # Then spaces are trimmed, and is the string starts with double-quotes
    # then also first and last double-quote are removed.
    d="$(sed -n -E "$range s/.*$4[[:space:]]+(.*)/\1/ p" "$2")"
    if [ "${d:0:1}" = '"' ]; then
        d="${d:1}"    # Drop the first character ("), and the last
        d="${d%\"*}"  #   double-quote and everything after it
    else
        d="$(sed -E 's/[[:space:]]*$//' <<<"$d")"  # Remove trailing space
    fi
    _returnvar="$d"
}


# --- checkCommand() command package -------------------------------------8<--1
# INFO
#    Check if command $1 exists; if not, then show an error message
# INPUT
#    $1 (str) the command name
#    $2 (str) the package providing the command (apt-get install $package)
# OUTPUT
#    Stdout   none
#    Stderr   possibly an error message via call to error()
#    File     none
#    Variable none
checkCommand() {
    if ! command -v "$1" >/dev/null; then
        error "Command $1 is not found!" "(In Debian, install package: $2)"
    fi
}


# --- checkConnectionToZen() ---------------------------------------------8<--1
# INFO
#    Uses command 'mtp-connect' to check if this script can communicate with
#    Zen.  If connection is ok, returns. Else gives an error message and exits.
#
#    For me, a usual communication problem with Zen is KDE system taking over
#    the USB connection immediately when Zen is zonnected to the computer.
#    When this is the case, 'mtp-*' commands return an error  message, which
#    has string "...reports device is busy...".  Existence of this string is
#    used to detect the communication problem.  As a fix, this has worked for
#    me: command 'usbreset' resets the connection and releases Zen for 'mtp-*'
#    commands  =>  --reset-usb option.
# INPUT
#    none
# OUTPUT
#    Stdout   none
#    Stderr   possibly an error message
#    File     none
#    Variable none
checkConnectionToZen() {
    checkCommand mtp-connect mtp-tools

    local response

    # Normal response to mtp-connect command:
    # - -8<- - - - - - - - - - - - - - - - - - - -
    # libmtp version: 1.1.19
    #
    # Device 0 (VID=041e and PID=4161) is a Creative ZEN Mozaic.
    # Usage: connect <command1> <command2>
    # Commands: --delete [filename]
    #           --sendfile [source] [destination]
    #           --sendtrack [source] [destination]
    #           --getfile [source] [destination]
    #           --newfolder [foldername]
    # - - - - - - - - - - - - - - - - - - - ->8- -
    #
    # Response to mtp-connect command when connection busy:
    # - -8<- - - - - - - - - - - - - - - - - - - -
    # libmtp version: 1.1.19
    #
    # Device 0 (VID=041e and PID=4161) is a Creative ZEN Mozaic.
    # libusb_claim_interface() reports device is busy, likely in use by GVFS or
    # KDE MTP device handling alreadyLIBMTP PANIC: Unable to initialize device
    # No devices.
    # - - - - - - - - - - - - - - - - - - - ->8- -
    info 'Checking connection to Zen...'
    cmd response mtp-connect  # Capture both stdout and stderr

    if grep -q 'reports device is busy' <<< "$response"; then
        printf '%s\n' "$response" >&2
        error 'Can not connect to Zen, connection is busy.'  \
              'Using --reset-usb option may help'
    elif grep -q 'No devices.' <<< "$response"; then
        printf '%s\n' "$response" >&2
        error 'Can not connect to Zen, is Zen on and connected?'
    fi
}


# --- collectDataFromZen() -----------------------------------------------8<--1
# INFO
#    Use 'mtp-folders' and 'mtp-filetree' commands to collect file and folder
#    information from Zen.  Collect the information into indexed (1,2,3...)
#    arrays: $ZNAME[] (names), $ZID[] (ID numbers), $ZTYPE[] ("file" or
#    "folder"), and $ZLEVEL (depth level).  Remove any previous data in arrays.
# INPUT
#    none
# OUTPUT
#    Stdout   message about "taking a while"
#    Stderr   none
#    File     none
#    Variable filled arrays $ZNAME[], $ZID[], $ZTYPE[], $ZLEVEL, and ZID2INDEX
collectDataFromZen() {
    checkCommand mtp-folders mtp-tools
    checkCommand mtp-filetree mtp-tools

    local line         # Line of 'mtp-filetree' output that is being processed
    local i=1          # Index of the arrays
    local folderList   # List of all folders and their ID numbers
    local fileList     # List of all folders and files and their ID numbers

    ZNAME=()  # Make arrays empty
    ZID=()
    ZTYPE=()
    ZLEVEL=()
    ZID2INDEX=()

    info 'Collecting information from Zen, which takes a while - please wait...'

    cmd folderList mtp-folders
    cmd fileList mtp-filetree

    # Processing the output of "mtp-filetree" command.
    # File/folder lines have this format:
    # <zero or more spaces><one to six digits><space><file/folder name>
    while IFS= read -r line; do
        if [[ "${line:0:1}" =~ [^[:space:][:digit:]] ]]; then continue; fi
        read -r "ZID[i]" "ZNAME[i]" <<< "$line"
        ZLEVEL[i]=$(( (${#line} - ${#ZID[i]} - ${#ZNAME[i]} - 1) / 2 ))
        if grep -q -P "^${ZID[i]}\t" <<< "$folderList"; then  # If folder
            ZTYPE[i]='folder'
        else  # Else $line is a file
            ZTYPE[i]='file'
        fi
        ZID2INDEX[${ZID[i]}]="$i"
        i=$((i + 1))
    done <<< "$fileList"
}


# --- searchIndex() name id type -----------------------------------------8<--1
# INFO
#    Search through files/subfolders under folder $2 for index of name $1 and
#    type $3.  If $2=0, then search root level.
# INPUT
#    $1 (str) the name of searched file or folder
#    $2 (int) the ID number of folder under which $1 searched, 0 = root level
#    $3 (str) type of searched item, "file" or "folder"
# OUTPUT
#    Stdout   show 0, if no match found; else show index of matching file/folder
#    Stderr   none
#    File     none
#    Variable none
searchIndex() {
    local i
    local level

    if (($2 == 0)); then  # If the file/folder is on the root level
        i=0
        level=0
    else  # Else file/folder is on some deeper level
        i="${ZID2INDEX[$2]}"  # The index for the $ID
        # Error if $2 does not exist or is not a folder
        if [ -z "$i" ]; then errorNum 6 "$2"; fi
        if [ "${ZTYPE[i]}" != 'folder' ]; then errorNum 5 "$2"; fi
        level=$((ZLEVEL[i] + 1))  # The level of the file/folder
    fi
    for ((i=i+1; i<=${#ZID[@]}; i++)); do
        if ((ZLEVEL[i] < level)); then  # If all files/folders checked
            break
        fi
        if [ "${ZLEVEL[i]}" -eq "$level" ] && \
           [ "${ZNAME[i]}" = "$1" ] &&        \
           [ "${ZTYPE[i]}" = "$3" ]; then  # If the file/folder is found
            printf '%i\n' "$i"
            return
        fi
    done
    printf '0\n'
}


# --- echoListLine() id level name ---------------------------------------8<--1
# INFO
#    Show a line from file/folder list.  Each line is limited / space-padded to
#    terminal width and terminated with ansi colors reset.
# INPUT
#    $1 (str) padding + ID number, successive lines should have the same width
#             for this parameter as it is displayed as it is
#    $2 (int) the level in dir tree
#    $3 (str) the name string
# OUTPUT
#    Stdout   none
#    Stderr   none
#    File     none
#    Variable none
echoListLine() {
    # line: $1 + space + (3 spaces * level $2) + space + name $3 + padding
    local line="$1 ${SPACES:0:$2*3} $3$SPACES"

    printf '%s\e[0m\n' "${line:0:$TERMWIDTH}"  # Limit printing to term width
}


# --- showTree() depth showfiles parentid --------------------------------8<--1
# INFO
#    Show subfolders/files of $3.  If $3=0 or $3='', then parent is root.
# INPUT
#    $1 (int) $1=0: list all folder levels from $3; $1=N: list N levels
#    $2 (boo) $2='true': list both files and folders; else list only folders
#    $3 (int) the ID number of the folder which subfolders are listed;
#             if $3=0 or missing, then parent is root
# OUTPUT
#    Stdout   show the files/folders
#    Stderr   none
#    File     none
#    Variable none
showTree() {
    local parentId="${3:-0}"  # If $3 not given, set parentId=0
    local i                   # Index to arrays
    local level               # Level of folder $parentId
    local tmp

    if [ -n "$4" ]; then errorNum 2; fi       # Error if parameter exists
    if [ -z "$3" ]; then errorNum 3 'ID'; fi  # Check ID

    if ((parentId == 0)); then
        i=0
        level=0
    else
        i="${ZID2INDEX[parentId]}"
        # Error if $parentId does not exist or is not a folder
        if [ -z "$i" ]; then errorNum 6 "$parentId"; fi
        if [ "${ZTYPE[i]}" != 'folder' ]; then errorNum 5 "$parentId"; fi
        level="${ZLEVEL[i]}"
    fi
    if [ "$2" = 'true' ]; then
        echoListLine '     ID' "$level" 'FOLDER AND FILE NAMES'
        echoListLine '=======' "$level" '====================='
    else
        echoListLine '     ID' "$level" 'FOLDER NAME'
        echoListLine '=======' "$level" '==========='
    fi

    if ((parentId > 0)); then  # Don't show parent folder if $parentId=0
        tmp="      ${ZID[i]}"
        echoListLine "${tmp: -7}" "${ZLEVEL[i]}" "${ZNAME[i]}"
    fi

    for ((i=i+1; i<=${#ZID[@]}; i++)); do
        # If parent ID not 0, check if all subfolders/files are already shown
        if ((parentId != 0)) && ((ZLEVEL[i] <= level)); then
            break
        fi
        # If not showing all levels, check if this level is to be shown
        if (($1 != 0)) && ((ZLEVEL[i] > level + $1)); then
            continue
        fi
        # Show this, if showing also files, or this is a folder
        if [ "$2" = 'true' ] && [ "${ZTYPE[i]}" = 'file' ]; then
            tmp="      ${ZID[i]}"
            echoListLine "${tmp: -7}" "${ZLEVEL[i]}" "${ZNAME[i]}"
        elif [ "${ZTYPE[i]}" = 'folder' ]; then
            tmp="      ${ZID[i]}"
            echoListLine "${tmp: -7}" "${ZLEVEL[i]}" "${ZNAME[i]}/"
        fi
    done
}


# --- loadArt() albumfilename artfilename --------------------------------8<--1
# INFO
#    Loads/connects an album art image to an album.  $1 must exist in folder
#    "My Albums". $2 must be a suitable image for an album art.  According some
#    information in the net, Zen Mozaic has the max size about 20 kB for
#    an album art image. Zen Mozaic has 128x160 display, thus higher resolution
#    seems to be waste.
# INPUT
#    $1 (str) the file name of the album in folder /My Albums
#    $2 (str) the file name of the album art image
# OUTPUT
#    Stdout   show output of mtp-thumb command
#    Stderr   possibly error message
#    File     none
#    Variable none
loadArt() {
    checkCommand mtp-thumb mtp-tools

    local indexMyAlbums
    local indexAlbumFile

    if [ -z "$1" ]; then errorNum 3 'ALBUM-FILE'; fi     # Error if param empty
    if [ -z "$2" ]; then errorNum 3 'ALBUM-ART-FILE'; fi # Error if param empty
    if [ -n "$3" ]; then errorNum 2; fi         # Error if parameter exists
    if [ ! -f "$2" ]; then errorNum 4 "$2"; fi  # Error if file doesn't exist

    indexMyAlbums=$(searchIndex 'My Albums' 0 'folder')
    if ((indexMyAlbums == 0)); then errorNum 1 'My Albums'; fi

    indexAlbumFile=$(searchIndex "$1" "${ZID[indexMyAlbums]}" 'file')
    if ((indexAlbumFile == 0)); then errorNum 4 "$1"; fi

    # mtp-thumb command usage:
    # thumb -i <fileid/trackid> <imagefile>
    cmd '' mtp-thumb -i "${ZID[indexAlbumFile]}" "$2"
}


# --- createFolder() returnvar name parentid update ----------------------8<--1
# INFO
#    If folder doesn't already exist, create folder $2 under $3.  Arrays ZID[],
#    ZNAME[], ZTYPE[], ZLEVEL[] & ZID2INDEX[] must be up-to-date at calling
#    createFolder().  If $1 is non-empty, returns the ID of created folder to
#    variable named $1.
# INPUT
#    $1 (str) caller's variable name, where the return data is written
#    $2 (str) name of the new folder
#    $3 (int) folder ID where the new folder is created
#    $4 (boo) false: don't update the file/folder arrays after creating new
#             folder; else: update arrays
# OUTPUT
#    Stdout   possibly output of mtp-newforder command;
#             see also collectDataFromZen()
#    Stderr   none
#    File     none
#    Variable stores the ID of the folder to variable given as $4 (name ref)
createFolder() {
    checkCommand mtp-newfolder mtp-tools

    local i
    local cmdoutput

    if [ -n "$1" ]; then
        local -n _returnvar="$1"  # Set the nameref for returning the folder ID
    fi

    if [ -z "$2" ]; then errorNum 3 NAME; fi       # Error if parameter empty
    if [ -z "$3" ]; then errorNum 3 PARENT-ID; fi  # Error if parameter empty
    if [ -n "$5" ]; then errorNum 2; fi            # Error if parameter exists

    if (($3 != 0)); then
        i="${ZID2INDEX[$3]}"
        # Error if $3 does not exist or is not a folder
        if [ -z "$i" ]; then errorNum 6 "$3"; fi
        if [ "${ZTYPE[i]}" != 'folder' ]; then errorNum 5 "$3"; fi
    fi
    i=$(searchIndex "$2" "$3" 'folder')
    if ((i != 0)); then  # if folder already exists
        if [ -n "$1" ]; then
            _returnvar="${ZID[i]}"
        fi
        return
    fi

    # mtp-newfolder command usage:
    # newfolder name <parent> <storage>
    # parent = parent folder or 0 to create the new folder in the root dir
    # storage = storage ID or 0 to create the new folder on the primary storage
    #
    # Output example of mtp-newfolder command:
    # - -8<- - - - - - - - - - - - - - - - - - - -
    # libmtp version: 1.1.19
    #
    # Device 0 (VID=041e and PID=4161) is a Creative ZEN Mozaic.
    # New folder created with ID: 139586
    # - - - - - - - - - - - - - - - - - - - ->8- -
    cmd cmdoutput mtp-newfolder "$2" "$3" 0
    printf '%s\n' "$cmdoutput"
    if [ -n "$1" ]; then
        _returnvar="$(sed -n '/with ID:/ s/.*with ID: //p' <<< "$cmdoutput")"
    fi

    if [ "$4" != 'false' ]; then collectDataFromZen; fi
}


# --- loadTrack() file title artist albumartist... -----------------------8<--1
#                 ...album genre tracknum year
# INFO
#    Loads music track file to Zen.  The file $1 is loaded to path
#    /Music/$albumartist/[$year: ]$album/[$tracknum: ]$title.mp3
# INPUT
#    $1 (str) path/filename of the track file to be loaded
#    $2 (str) music title, must not be null; is also part of track file name
#    $3 (str) artist name; if null, then $4 is used as $artist; both $3 and $4
#             must not be null
#    $4 (str) album artist name; if null, then $3 is used as $albumartist;
#             both $3 and $4 must not be null; is also part of track file path
#    $5 (str) album name, must not be null; is also part of track file path
#    $6 (str) genre
#    $7 (str) track number in the album; if given, becomes also part of
#             track file name
#    $8 (str) year of the album
# OUTPUT
#    Stdout   show output of mtp-sendtr command
#    Stderr   possibly error message
#    File     none
#    Variable none
loadTrack() {
    checkCommand mtp-sendtr mtp-tools

    local file="$1"
    local artist="$3"
    local albumartist="$4"
    local targetpath
    local extension="${file##*.}"
    local index
    local idPerformer

    if [ -z "$file" ]; then errorNum 3 'file'; fi       # Check file
    if [ -z "$2" ]; then errorNum 3 'track title'; fi   # Check title
    if [ -z "$5" ]; then errorNum 3 'album name'; fi    # Check album name
    if [ ! -f "$file" ]; then errorNum 4 "$file"; fi    # If file doesn't exist

    if [ -z "$artist" ] && [ -z "$albumartist" ]; then  # Check $3 & $4
        error 'Loading track requires providing artist or albumartist or both'
    elif [ -z "$artist" ] && [ -n "$albumartist" ]; then
        artist="$albumartist"
    elif [ -n "$artist" ] && [ -z "$albumartist" ]; then
        albumartist="$artist"
    fi

    # Make sure that /My Albums folder exists (or create it)
    createFolder '' "My Albums" 0 'false'

    # Make folder /Music/$albumartist and get it's ID to idPerformer
    createFolder idPerformer "$albumartist" "$MUSICID"

    if [ -n "$8" ]; then
        if [ -n "$7" ]; then
            targetpath="/Music/$albumartist/$8: $5/$7: $2.$extension"
        else
            targetpath="/Music/$albumartist/$8: $5/$2.$extension"
        fi
        # Make folder /Music/$albumartist/$album
        createFolder '' "$8: $5" "$idPerformer"

    else
        if [ -n "$7" ]; then
            targetpath="/Music/$albumartist/$5/$7: $2.$extension"
        else
            targetpath="/Music/$albumartist/$5/$2.$extension"
        fi
        # Make folder /Music/$albumartist/$album
        createFolder '' "$5" "$idPerformer"
    fi

    # mtp-sendtr command usage:
    # sendtr [ -D debuglvl ] [ -q ]
    # -t <title> -a <artist> -A <Album artist> -w <writer or composer>
    # -l <album> -c <codec> -g <genre> -n <track number> -y <year>
    # -d <duration in seconds> -s <storage_id> <local path> <remote path>
    # (-q means the program will not ask for missing information.)
    cmd '' mtp-sendtr -q -t "$2" -a "$artist" -A "$albumartist" -l "$5"  \
                      -g "$6" -n "$7" -y "$8" -s 0 "$file" "$targetpath"
}


# --- loadAlbum() file ---------------------------------------------------8<--1
# INFO
#    Process album flac file $1.flac to MP3 tracks and load them to Zen.
#    Also .cue file $1.cue must exist.  If file $1-art.jpg exists, is is loaded
#    to ZEN as album art.
# INPUT
#    $1 (str) the file name of the album (without extension)
# OUTPUT
#    Stdout   show output of various command that are executed
#    Stderr   none
#    File     none
#    Variable none
loadAlbum() {
    checkCommand shnsplit shntool
    checkCommand flac flac
    checkCommand ffmpeg ffmpeg
    checkCommand mtp-folders mtp-tools

    local album        # Album name in cue sheet file
    local albumartist  # Album artist name in cue sheet file
    local artist       # Track artist in cue sheet file
    local f            # File name variable in loop below
    local genre        # Album genre in cue sheet file
    local tmpdir
    local title        # Track title in cue sheet file
    local trackNum
    local year         # Album year in cue sheet file

    if [ -z "$1" ]; then errorNum 3 'FILE'; fi  # Error if parameter empty
    if [ -n "$2" ]; then errorNum 2; fi         # Error if parameter exists
    if [ ! -f "$1.flac" ]; then errorNum 4 "$1.flac"; fi  # Error if files...
    if [ ! -f "$1.cue" ]; then errorNum 4 "$1.cue"; fi    # ...do not exist

    tmpdir="$(mktemp -d)"

    # Split music album $1.flac into tracks trackXX.flac
    info "Splitting album to trackXX.flac files in $tmpdir"
    cmd '' shnsplit -f "$1.cue" -d "$tmpdir" -t track%n -o flac "$1.flac"

    # Convert all trackXX.flac files in tmpdir to *.mp3 files
    for f in "$tmpdir"/track*.flac; do
        info "Converting $f to .mp3"
        cmd '' ffmpeg -i "$f" "$tmpdir/$(basename "$f" .flac).mp3"
    done

    # Get album info from .cue file
    albumData album "$1.cue" TITLE
    albumData albumartist "$1.cue" PERFORMER
    albumData genre "$1.cue" GENRE
    albumData year "$1.cue" DATE

    for f in "$tmpdir"/track*.mp3; do
        info "Loading music file $f"
        trackNum=${f:${#tmpdir}+6:2}
        trackData title "$1.cue" "$trackNum" TITLE
        trackData artist "$1.cue" "$trackNum" PERFORMER
        loadTrack "$f" "$title" "$artist" "$albumartist" "$album" "$genre"  \
                  "$trackNum" "$year"
    done


    if [ -f "$1-art.jpg" ]; then
        collectDataFromZen  # Update array data
        info 'Loading album art file'
        loadArt "$album.alb" "$1-art.jpg"
    else
        info 'Album art file is not found.'
    fi

    info "Temporary files are found in $tmpdir"
}


# --- getFile() id name -------------------------------------------------8<--1
# INFO
#    Download file with id $1 from Zen and store it to current directory
#    using name $2.  Will not download, if the file $2 already exists.
# INPUT
#    $1 (int) the id number of the file to be downloaded
#    $2 (str) the name for the stored file
# OUTPUT
#    Stdout   show information from mtp-getfile command
#    Stderr   none
#    File     none
#    Variable none
getFile() {
    checkCommand mtp-getfile mtp-tools

    if [ -z "$1" ]; then errorNum 3 'ID'; fi        # Error if parameter empty
    if [ -z "$2" ]; then errorNum 3 'FILE-NAME'; fi # Error if parameter empty
    if [ -n "$3" ]; then errorNum 2; fi             # Error if parameter exists
    if [ -f "$2" ]; then error "File $2 already exists."; fi

    # mtp-getfile command usage:
    # getfile [<deviceid>] <fileid/trackid> <filename>
    cmd '' mtp-getfile "$1" "$2"
}


# --- deleteId() id ------------------------------------------------------8<--1
# INFO
#    Delete the file or folder in Zen defined by id number $1.
# INPUT
#    $1 (int) the id number of the item to be deleted
# OUTPUT
#    Stdout   show information from mtp-delfile command
#    Stderr   none
#    File     none
#    Variable none
deleteId() {
    checkCommand mtp-delfile mtp-tools

    if [ -z "$1" ]; then errorNum 3 ID; fi  # Error if parameter empty
    if [ -n "$2" ]; then errorNum 2; fi     # Error if parameter exists

    # mtp-delfile command usage:
    # delfile [<deviceid>] -n <fileid/trackid> | -f <filename> ...
    cmd '' mtp-delfile -n "$1"
}


# --- limitValue() value low-limit high-limit ----------------------------8<--1
# INFO
#    Limit value of value in $1: $2 <= $1 <= $3.  $1 is the name of the
#    variable holding the value; it is handled as nameref.
# INPUT
#    $1 (str) name of the variable, which value is to be limited
#    $2 (int) low limit for the value
#    $3 (int) high limit for the value
# OUTPUT
#    Stdout   none
#    Stderr   none
#    File     none
#    Variable limits the value of the variable, which name is given as $1
limitValue() {
    local -n _value="$1"

    if (($2 > _value)); then
        _value="$2"
    elif (($3 < _value)); then
        _value="$3"
    fi
}


# --- refreshFileArray() -------------------------------------------------8<--1
# INFO
#    Read data from Zen into arrays.  Called by zenBrowse().
# INPUT
#    none
# OUTPUT
#    Stdout   interactive operation
#    Stderr   none
#    File     none
#    Variable updates $ZID[], $ZNAME[], $ZTYPE[], $ZLEVEL[], $ZID2INDEX[],
#    $zInfo[], $maxIndex and $maxDisplayIndexTop
refreshFileArray() {
    local commandOutput
    local id
    local index

    collectDataFromZen

    maxIndex=${#ZID[@]}
    maxDisplayIndexTop=$((maxIndex - termHeight + 1))  # Max of displayIndexTop

    cmd commandOutput mtp-files
    while IFS= read -r id; do
        index="${ZID2INDEX[id]}"
        zInfo[index]="$(sed -n "/^F.* $id$/ {:L; p; n; /^[FO]/ ! bL}"  \
                        <<<"$commandOutput")"
        zInfo[index]+=$'\n'
    done < <(sed -n '/^File/ s/.*ID: //p' <<<"$commandOutput")

    cmd commandOutput mtp-tracks
#    for id in $(sed -n '/^Track/ s/.*ID: //p' <<<"$commandOutput"); do
    while IFS= read -r id; do
        index="${ZID2INDEX[id]}"
        zInfo[index]+=$'\n'
        zInfo[index]+="$(sed -n "/^T.* $id$/ {:L; p; n; /^[TO]/ ! bL}"  \
                         <<<"$commandOutput")"
        zInfo[index]+=$'\n'
    done < <(sed -n '/^Track/ s/.*ID: //p' <<<"$commandOutput")

    cmd commandOutput mtp-playlists
#    for id in $(sed -n '/^Playlist/ s/.*ID: //p' <<<"$commandOutput"); do
    while IFS= read -r id; do
        index="${ZID2INDEX[id]}"
        zInfo[index]+=$'\n'
        zInfo[index]+="$(sed -n "/^P.* $id$/ {:L; p; n; /^[PO]/ ! bL}"  \
                         <<<"$commandOutput")"
        zInfo[index]+=$'\n'
    done < <(sed -n '/^Playlist/ s/.*ID: //p' <<<"$commandOutput")
}


# --- zenBrowse() --------------------------------------------------------8<--1
# INFO
#    Interactive music/file handling.
# INPUT
#    none
# OUTPUT
#    Stdout   interactive operation
#    Stderr   none
#    File     none
#    Variable none
zenBrowse() {
    local f=1             # Selected file array index
    local i               # Temporary index
    local tmp
    local termHeight="$(($(tput lines) - 2))"  # Terminal height - status line
    local maxIndex            # Number of lines (max index) in the file arrays
    local displayIndexTop=1   # Index of the 1st line displayed (top of screen)
    local maxDisplayIndexTop  # Max value for displayIndexTop
    local displayIndexBottom  # Index of the last line displayed (bottom)
    declare -a zInfo      # One-based array of file/track info

    refreshFileArray

    # Scroll screen up to keep current info in the screen buffer
    for ((i=termHeight; i>=0; i--)); do printf '\n'; done;

    while true; do
        # 1 <= $f <= $maxIndex
        limitValue 'f' 1 "$maxIndex"

        if ((maxIndex <= termHeight)); then
            # If file list is shorter than terminal height, add empty rows
            for ((i=maxIndex - termHeight; i<0; i++)); do printf '\n'; done
            displayIndexTop=1
            displayIndexBottom="$maxIndex"
        else
            displayIndexTop="$((f - (termHeight / 2)))"
            limitValue 'displayIndexTop' 1 "$maxDisplayIndexTop"
            displayIndexBottom=$((displayIndexTop + termHeight - 1))
        fi

        printf '\e[H'  # Move cursor to top - left
        for ((i=displayIndexTop; i<=displayIndexBottom; i++)); do
            tmp="      ${ZID[i]}"
            if ((i == f)); then
                printf '\e[93m'
                tmp="-> ${tmp: -7}"
            elif [ "${ZTYPE[i]}" = 'folder' ]; then
                printf '\e[34m'
                tmp="   ${tmp: -7}"
            else
                tmp="   ${tmp: -7}"
            fi
            if [ "${ZTYPE[i]}" = 'folder' ]; then
                echoListLine "$tmp" "${ZLEVEL[i]}" "${ZNAME[i]}/"
            else
                echoListLine "$tmp" "${ZLEVEL[i]}" "${ZNAME[i]}"
            fi
        done

        # --- Display bottom line ---
        for ((i=TERMWIDTH; i>0; i--)); do printf '-'; done
        tmp="$((${#ZID[f]} + ${#ZNAME[f]} + ${#ZTYPE[f]}))"
        # If terminal wide enough to display all...
        if ((30 + ${#f} + ${#maxIndex} + tmp <= TERMWIDTH)); then
            tmp="${ZTYPE[f]@U}: ${ZID[f]} ${ZNAME[f]}   $f/$maxIndex$SPACES"
            tmp="${tmp:0:TERMWIDTH-20}Esc = quit, H = help"
        # Else drop 'Esc = quit' and test if terminal is now wide enough...
        elif ((18 + ${#f} + ${#maxIndex} + tmp <= TERMWIDTH)); then
            tmp="${ZTYPE[f]@U}: ${ZID[f]} ${ZNAME[f]}   $f/$maxIndex$SPACES"
            tmp="${tmp:0:TERMWIDTH-8}H = help"
        # Else drop '$f/$maxIndex' and test if terminal is now wide enough...
        elif ((14 + tmp <= TERMWIDTH)); then
            tmp="${ZTYPE[f]@U}: ${ZID[f]} ${ZNAME[f]}$SPACES"
            tmp="${tmp:0:TERMWIDTH-8}H = help"
        # Else if terminal width less than 14, don't display anything
        elif ((TERMWIDTH < 14)); then
            tmp="${SPACES:0:TERMWIDTH}"
        # Else shorten name and display what fits...
        else
            tmp="${ZTYPE[f]@U}: ${ZID[f]} ${ZNAME[f]}"
            tmp="${tmp:0:TERMWIDTH-14}...   H = help"
        fi
        printf '\n\e[93m%s\e[0m\e[?25l\e[1A' "$tmp" # Hide cursor & one row up

        # --- Wait for a key press and meanwhile check terminal size change ---
        while true; do
            read -r -n1 -s -t0.5 i
            if [ "${#i}" -gt 0 ]; then
                # A key pressed: wait for multi-byte key data, if any
                while read -r -n1 -t.01 tmp; do i="$i$tmp"; done
                break
            else
                # Check if terminal height is changed
                i="$(($(tput lines) - 2))"
                if ((i != termHeight)); then
                    termHeight="$i"
                    maxDisplayIndexTop=$((maxIndex - termHeight + 1))
                    i=""
                    break
                fi
                # Check if terminal width is changed
                i="$(tput cols)"
                if ((i != TERMWIDTH)); then
                    while [ "${#SPACES}" -lt "$i" ]; do SPACES+="$SPACES"; done
                    TERMWIDTH="$i"
                    i=""
                    break
                fi
            fi
        done

        printf '\e[?25h\e[1B'  # Show cursor + put it one row down
        case "$i" in
            q|Q|$'\e')    break                       ;;  # Q, Esc
            k|K|$'\e[A')  f=$((f - 1))                ;;  # Up
            j|J|$'\e[B')  f=$((f + 1))                ;;  # Down
            $'\e[5~')     f=$((f - (termHeight / 2))) ;;  # PgUp
            $'\e[6~')     f=$((f + (termHeight / 2))) ;;  # PgDn
            c|C)
                printf '\n\n'
                if [ "${ZTYPE[f]}" = 'file' ]; then
                    printf 'This is a file, cannot create a folder under '
                    printf 'a file.  Press enter to continue '
                    read -r
                else
                    printf 'Give the folder name to be created\n'
                    printf '(to cancel: press enter without writing anything): '
                    read -r tmp
                    if [ -n "$tmp" ]; then
                        printf 'Creating...\n'
                        createFolder '' "$tmp" "${ZID[f]}" 'false'
                        refreshFileArray
                    fi
                fi
                ;;
            d|D)
                printf '\n\n'
                if [ "${ZTYPE[f]}" = 'folder' ]; then
                    printf 'This is a folder, cannot download a folder.'
                    printf '  Press enter to continue '
                    read -r
                elif [ -f "./${ZNAME[f]}" ]; then
                    printf 'Selected file already exists in current folder.'
                    printf '  Press enter to continue '
                    read -r
                else
                    printf 'Selected file will be downloaded to current folder.'
                    printf ' press y + enter; anything else + enter to cancel '
                    read -r tmp
                    if [ "$tmp" = 'y' ] || [ "$tmp" = 'Y' ]; then
                        printf 'Downloading...\n'
                        getFile "${ZID[f]}" "${ZNAME[f]}"
                    fi
                fi
                ;;
            h|H)
                printf '\n\nKEYS\n'
                printf '      Q, <ESC>   Quit\n\n'
                printf '      K, <Up>    Move to previous line\n'
                printf '      J, <Down>  Move to next line\n'
                printf '      <PgUp>     Move half screen up\n'
                printf '      <PgDn>     Move half screen down\n\n'
                printf '      I          If file: show detailed information\n'
                printf '      C          Create a subfolder under a folder\n'
                printf '      D          Download selected file from Zen\n'
                printf '      X, <Del>   Delete selected file/folder\n\n'
                printf 'For other ways to run this script, use --help option '
                printf 'on command line\n\n\n'
                read -r -p 'Press enter to continue '
                ;;
            i|I)
                if [ "${ZTYPE[f]}" = 'file' ]; then
                    printf '\n\n%s\n' "${zInfo[f]}"
                    read -r -p 'Press enter to continue '
                fi
                ;;
            x|X|$'\e[3~') # X, Del
                if [ "${ZTYPE[f]}" = 'file' ]; then
                    printf '\n\nTo delete file ID=%i,' "${ZID[f]}"
                else
                    printf '\n\nEverything in this folder will be deleted!\n'
                    printf 'To delete folder ID=%i,' "${ZID[f]}"
                fi
                printf ' press y + enter; anything else + enter to cancel '
                read -r tmp
                if [ "$tmp" = 'y' ] || [ "$tmp" = 'Y' ]; then
                    printf 'Deleting...\n'
                    deleteId "${ZID[f]}"
                    refreshFileArray
                fi
                ;;
        esac
#        printf '\n'
    done
    printf '\n'
}


# --- MAIN ---------------------------------------------------------------8<--1

TERMWIDTH="$(tput cols)"  # Terminal width
while [ "${#SPACES}" -lt "$TERMWIDTH" ]; do SPACES+="$SPACES"; done

CMD=''  # Command given on the command line

# --- Read the command line
while [ "$#" -ne 0 ]; do
    case "$1" in
        -r|--reset-usb)
            checkCommand usbreset usbutils
            usbreset 'Creative ZEN Mozaic'
            ;;
        -h|--help)
            printf '%s\n' "$HELP_TXT"
            exit 0
            ;;
        --copyright|--licence|--license|--version)
            printf '%s\n' "$LICENCE_VERSION_TXT"
            exit 0
            ;;
        -*) errorNum 7 "$1" ;;
        *)
            if   [ -z "$CMD" ];  then CMD="$1"
            elif [ -z "${P1+.}" ]; then P1="$1"  # If $Px unset, assign value
            elif [ -z "${P2+.}" ]; then P2="$1"
            elif [ -z "${P3+.}" ]; then P3="$1"
            elif [ -z "${P4+.}" ]; then P4="$1"
            elif [ -z "${P5+.}" ]; then P5="$1"
            elif [ -z "${P6+.}" ]; then P6="$1"
            elif [ -z "${P7+.}" ]; then P7="$1"
            elif [ -z "${P8+.}" ]; then P8="$1"
            else errorNum 2
            fi
            ;;
    esac
    shift
done

checkConnectionToZen

if [ -z "$CMD" ]; then zenBrowse; exit 0; fi

collectDataFromZen

MUSICID=$(searchIndex 'Music' 0 'folder')       # Index to the arrays
if ((MUSICID == 0)); then errorNum 1 Music; fi  # Index = 0, if not found
MUSICID="${ZID[MUSICID]}"                       # MUSICID holds now ID number

case $CMD in
    create-folder)
        createFolder '' "$P1" "$P2" 'false' "$P3"
        ;;
    delete-id)
        deleteId "$P1" "$P2"
        ;;
    get-file)
        getFile "$P1" "$P2" "$P3"
        ;;
    list-albums)
        showTree 0 'false' "$P1" "$P2"
        ;;
    list-folders)
        showTree 0 'false' "${P1:-0}" "$P2"
        ;;
    list-music)
        showTree 0 'true' "$MUSICID" "$P1"
        ;;
    list-performers)
        showTree 1 'false' "$MUSICID" "$P1"
        ;;
    list-tree)
        showTree 0 'true' "${P1:-0}" "$P2"
        ;;
    load-album)
        loadAlbum "$P1" "$P2"
        ;;
    load-art)
        loadArt "$P1" "$P2" "$P3"
        ;;
    load-track)
        loadTrack "$P1" "$P2" "$P3" "$P4" "$P5" "$P6" "$P7" "$P8"
        ;;
    *)
        error "Invalid command! $CMD"
        ;;
esac

