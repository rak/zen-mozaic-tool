# zen-mozaic-tool

Bash script: Linux shell tool for handling (music) files and folders in
Creative Zen Mozaic MP3 player.  Make sure that you have the
[required tools](#requirements) in your Linux system, connect to your Zen with
a USB cable and execute the script.

- (music) file and folder browsing
- folder creation
- file and folder deletion
- loading music to Zen Mozaic
- reading files from Zen Mozaic


## Contents

[Licence](#licence)  
[Description](#description)  
[Requirements](#requirements)  
[Installation](#installation)  
[About the code](#about-the-code)  



## Licence

Copyright Risto Karola 2022.  Licensed under the EUPL-1.2.
You may obtain a copy of the licence at
https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12.



## Description

### Usage

    zen-mozaic-tool.sh [options] [command [parameter(s)]]


### Options

    --copyright
           display version, copyright and licence information and exit
    -h, --help
           display this help text and exit
    --licence
           display version, copyright and licence information and exit
    -r, --reset-usb
           reset USB connection before detecting Zen, this might be needed if
           the system doesn't give the USB connection for mtp-tools commands;
           for example KDE tends to take over the connection when Zen is
           connected, and doesn't release it until USB connection is reseted
    --version
           display version, copyright and licence information and exit


### Commands and parameters

    If no command is given, the script operates interactively.

    Below several commands use ID as parameter.  ID is a number that identifies
    a file or a folder in Zen.  IDs are seen in list-* commands output and in
    interactive mode (when the script is run without a command).

    create-folder NAME PARENT-ID
           Create new folder in Zen under folder defined by PARENT-ID, use
           PARENT-ID = 0 to create the folder on the root level.  Name the
           folder as NAME.

    delete-id ID
           Delete file or folder in Zen; the file/folder is defined by ID.
           In case of folder, everything under it will also be deleted.

    get-file ID [PATH]FILE-NAME
           Download file with ID from Zen and store it as [PATH]FILE-NAME.
           The FILE is not downloaded, if [PATH]FILE-NAME already exists.

    list-albums PERFORMER-ID
           An alias for list-folders PERFORMER-ID.

    list-folders [FOLDER-ID]
           Show Zen folders under folder FOLDER-ID.  If FOLDER-ID is not given
           or FOLDER-ID = 0, then display all folders.

    list-music
           Lists folders and files under folder /Music in Zen.

    list-performers
           List music performers in Zen.  This is listing of the first level
           subfolders under folder /Music.

    list-tree [FOLDER-ID]
           Show Zen folders & files under folder FOLDER-ID.  If FOLDER-ID is
           not given or FOLDER-ID = 0, then display all folders and files.

    load-album [PATH]FILE
           Convert music in album FILE.flac to MP3 files and load them to
           folder /Music/<performer>/<album>/ in Zen.  Create also an album
           file in Zen: /My Albums/<album>.alb.  If album art FILE-art.jpg
           exists, it is connected to the album file.  This process requires
           existence of the cue sheet FILE.cue.

    load-art ALBUM-FILE [PATH]ALBUM-ART-FILE
           Connect the album art image ALBUM-ART-FILE to an album in Zen.  The
           album file must already exist in Zen: /My Albums/ALBUM-FILE.
           ALBUM-ART-FILE must be a suitable image for an album art.  Some
           information in the Net tells, that Zen Mozaic has the maximum size
           about 20 kB for an album art image.  Zen Mozaic has 128x160 display,
           thus higher resolution seems to be waste.

    load-track [PATH]FILE TITLE ARTIST ALBUMARTIST ALBUM GENRE TRACKNUM YEAR
           Load a music track FILE to Zen's path
           /Music/ALBUMARTIST/[YEAR: ]ALBUM/[TRACKNUM: ]TITLE.<extension>.
           Create album file /My Albums/ALBUM.alb, if it doesn't already exist.
           - FILE: music track file.
           - TITLE: track title.  This string must not be null.
           - ARTIST/ALBUMARTIST: track/album performer.  At least one of them
             is needed, thus both of them must not be a null strings.
           - ALBUM: album name.  This string must not be null.
           - GENRE: music genre.
           - TRACKNUM: track number in the album.  If given, the number is part
             of file name in Zen.
           - YEAR: album year.  If given, the year is part of album folder name
             in Zen.


### Exit codes

- 0 : No problems detected

- 1 : Problems detected, message output to stderr



## Requirements

This is a list of Debian backages, which are needed by the script (in addition
to normal linux tools):

- `mtp-tools` is a must for the script.

- `shntool`, `flac` and `ffmpeg` are needed by load-album command.

- `usbutils` is needed, if --reset-usb option is used.



## Installation

Nothing special here, normal single-file script.  Make it executable.



## About the code

zen-mozaic-tool.sh is a shell script, coded to run on Bash.  It is tested with
Creative Zen Mozaic version 1.06.01, mtp-tools 1.1.19 and Bash version 5.1.16.

